import { Box, Heading, Container } from '@chakra-ui/react';
import { FitnessCoachList } from '@components/FitnessCoachList.js';

const MarketplacePage = () => {
  return (
    <Container maxW='container.xl'>
      <Box py={10}>
        <Heading as='h1' size='2xl' mb={10} textAlign='center'>
          Fitness Coach Marketplace
        </Heading>
        <FitnessCoachList />
      </Box>
    </Container>
  );
};

export default MarketplacePage;