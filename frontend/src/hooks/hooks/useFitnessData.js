import { useQuery } from 'react-query';

export const useFitnessData = () => {
  const { data, isLoading, error } = useQuery('fitnessData', () =>
    fetch('/path/to/fitnessData.json').then((res) => res.json())
  );

  return { data, isLoading, error };
};