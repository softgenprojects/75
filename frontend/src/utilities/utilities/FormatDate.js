import { format } from 'date-fns';

export const FormatDate = (dateString) => {
  return format(new Date(dateString), 'MM/dd/yyyy');
};