import { Box, SimpleGrid, Text } from '@chakra-ui/react';
import { FitnessCoachCard } from './FitnessCoachCard';
import { useFitnessData } from '@hooks/useFitnessData';

export const FitnessCoachList = () => {
  const { data, isLoading, error } = useFitnessData();

  if (isLoading) return <Text>Loading...</Text>;
  if (error) return <Text>Error loading fitness coaches.</Text>;

  return (
    <Box>
      <SimpleGrid columns={{ base: 1, md: 2, lg: 3 }} spacing={10}>
        {data.map((coach) => (
          <FitnessCoachCard key={coach.id} name={coach.name} image={coach.image} specialty={coach.specialty} />
        ))}
      </SimpleGrid>
    </Box>
  );
};